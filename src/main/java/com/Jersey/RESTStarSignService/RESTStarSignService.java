package com.Jersey.RESTStarSignService;

import java.io.IOException;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;

public class RESTStarSignService {

    static final String BASE_URI = "http://localhost:9999/starsign";

    public static void main(String[] args) {
        try {
            HttpServer server = HttpServerFactory.create(BASE_URI);
            server.start();
            System.out.println("Press enter to stop server.");
            System.out.println("Go to: http://localhost:9999/");
            System.in.read();
            server.stop(0);
            System.out.println("Server stopped");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}