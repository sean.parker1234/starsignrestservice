package com.Jersey.RESTStarSignService;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/date")
public class RESTStarSignServiceDate {
    //used to communicate with 2nd web service (horoscopeREST)
    static final String REST_URI = "http://localhost:9998/";
    static final String HOROSCOPE_URI = "horoscope/";
//    @GET
//    @Path("/{day}/{month}")
//    @Produces(MediaType.TEXT_PLAIN)
//    public String getStarSign(@PathParam("day") double day, @PathParam("month") String month) {
//        String starSign = returnStarSign(day, month);
//        System.out.println(starSign);
//        return starSign;
//    }
    @GET
    @Path("/{day}/{month}")
    @Produces(MediaType.APPLICATION_JSON)
    public RESTResponse getStarSignJSON(@PathParam("day") double day, @PathParam("month") String month) {
        String starSign = returnStarSign(day,month);
        String joke = "meh";
        String horoscope = "myHoroscope";

        //gets horoscope from second web REST service
        horoscope = POSTStarSign(starSign);

        //returns joke
        JokeAPI jokeCommunicator = new JokeAPI();
        joke = jokeCommunicator.getJoke();

        RESTResponse responseToClient = new RESTResponse(joke, starSign, horoscope);

        return responseToClient;
    }

    public String returnStarSign(double day, String month) {
        String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        int index = -1;

        for (int i = 0; i < months.length; i++) {
            if (months[i].equals(month)) {
                index = i;
            }
        }
        String sign = getSign(index, day);
        return sign;
    }

    public String getSign(int index, double day) {
        System.out.println("Index:" + index + "\n Day:" + day);
        switch(index) {
            case 0:
                if(day >= 21) {
                    return "Aquarius";
                } else { return "Capricorn"; }
            case 1:
                if (day >= 19) {
                    return "Pisces";
                } else { return "Aquarius"; }
            case 2:
                if (day >= 21) {
                    return "Aries";
                } else { return "Pisces"; }
            case 3:
                if (day >= 21) {
                    return "Taurus";
                } else { return "Aries"; }
            case 4:
                if (day >= 22) {
                    return "Gemini";
                } else { return "Taurus"; }
            case 5:
                if (day >= 22) {
                    return "Cancer";
                } else { return "Gemini"; }
            case 6:
                if (day >= 23) {
                    return "Leo";
                } else { return "Cancer"; }
            case 7:
                if (day >= 24) {
                    return "Virgo";
                } else { return "Leo"; }
            case 8:
                if (day >= 23) {
                    return "Libra";
                } else { return "Virgo"; }
            case 9:
                if (day >= 24) {
                    return "Scorpio";
                } else { return "Libra"; }
            case 10:
                if (day >= 23) {
                    return "Sagittarius";
                } else { return "Scorpio"; }
            case 11:
                if (day >= 22) {
                    return "Capricorn";
                } else { return "Sagittarius"; }
            default:
                return "Incorrect month or day";
        }
    }

    protected String POSTStarSign(String starSign){

        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(REST_URI + HOROSCOPE_URI + starSign);
        ClientResponse response = service.put(ClientResponse.class, starSign);

        String serverResponse = this.getOutputAsText(service);
        return serverResponse;
    }

    private static String getOutputAsText(WebResource service) {
        return service.accept(MediaType.TEXT_PLAIN).get(String.class);
    }



}
