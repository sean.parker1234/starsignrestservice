package com.Jersey.RESTStarSignService;

public class RESTResponse {

    String joke;
    String starSign;
    String horoscope;

    public RESTResponse(String joke, String starSign, String horoscope){
        this.joke = joke;
        this.starSign = starSign;
        this.horoscope = horoscope;
    }

    public String getJoke(){
        return joke;
    }

    public String getStarSign(){
        return starSign;
    }

    public String getHoroscope(){
        return horoscope;
    }

    @Override
    public String toString(){
        return "RESTResponse [joke=" + joke + ", starSign=" + starSign +", horoscope="+ horoscope + "]";
    }
}
