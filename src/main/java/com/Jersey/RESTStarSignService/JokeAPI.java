package com.Jersey.RESTStarSignService;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JokeAPI {
    protected String getJoke(){
        URL apiURL;
        HttpURLConnection connection;
        InputStream input;

        //initialization for error checking
        String joke = "meh";

        JSONObject jokeJSON = null;
        try{
            //API url
            apiURL = new URL("https://sv443.net/jokeapi/v2/joke/Any?blacklistFlags=nsfw,racist,sexist");

            //open connection
            connection = (HttpURLConnection) apiURL.openConnection();
            connection.setRequestProperty("Accept", "Application/JSON");
            connection.setRequestProperty("Content-type", "Application/JSON");
            connection.setRequestMethod("GET");

            System.out.println("Server Response: " + connection.getResponseCode() + " connection was a success");

            if(connection.getResponseCode() != 200){
                throw new RuntimeException("Connection failed: " + connection.getResponseCode());
            }
            //handling data
            input = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input, "UTF-8"));

            String eachInputLine;
            StringBuffer response = new StringBuffer();

            while((eachInputLine = reader.readLine()) != null){
                response.append(eachInputLine);
            }
            reader.close();
            System.out.println(response.toString());
            //getting JSON response
            jokeJSON = new JSONObject(response.toString());
            joke = jokeJSON.getString("setup");
            joke = joke + "....\n" + jokeJSON.getString("delivery");
        }catch(IOException e){
            System.out.println("IOException for getting joke:");
            e.printStackTrace();
        }catch(JSONException es){
            System.out.println("JSON Parsing Exception:");
            es.printStackTrace();

            try{
                joke = jokeJSON.getString("joke");
            }catch(JSONException e){
                System.out.println("JSON Parsing Error, String joke was not found either:");
                e.printStackTrace();
            }

        }
        System.out.println(joke);
        return joke;
    }
}
